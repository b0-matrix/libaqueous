import 'package:libaqueous/src/api/client.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/sync/model/user_account_data.dart';

abstract class Room {
  Client get client;

  String get id;

  String get prevToken;

  bool get hasFullMembers;

  String get userMembership;

  int get notificationCount;

  int get highlightCount;

  List<Event> get stateEvents;

  List<UserAccountData> get accountDataEvents;

  List<Event> get timelineEvents;

  Future<void> postMessage(Map<String, dynamic> body);

  Future<void> postPlainText(String text);

  Future<void> postEmote(String text);

  Future<void> getPreviousEvents(int limit);

  Future<void> fetchMembers();
}
