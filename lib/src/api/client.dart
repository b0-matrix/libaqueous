import 'package:libaqueous/src/api/room.dart';
import 'package:libaqueous/src/rest/api.dart';
import 'package:libaqueous/src/rest/sync/model/user_account_data.dart';

abstract class ClientChanges {
  static const NoChange = 0x0;
  static const RoomsChange = 0x1;
  static const UsersChange = 0x2;
}

abstract class Client {
  MatrixAPI get api;

  String get homeserver;

  String get userID;

  String get token;

  String get transactionID;

  bool get isRunning;

  Map<String, Room> get rooms;

  List<UserAccountData> get accountDataEvents;

  Future<void> startSync();

  void stopSync();

  Future<void> logout();
}
