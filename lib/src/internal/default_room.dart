import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/api/room.dart';
import 'package:libaqueous/src/internal/default_client.dart';
import 'package:libaqueous/src/internal/default_timeline.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/event_type.dart';
import 'package:libaqueous/src/rest/events/model/tokens_chunk_events.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync_account_data.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync_state.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync_timeline.dart';
import 'package:libaqueous/src/rest/sync/model/user_account_data_fallback.dart';

abstract class RoomChanges {
  static const NoChange = 0x0;
  static const StateChanges = 0x02;
  static const AccountDataChange = 0x04;
  static const NotificationCountChange = 0x08;
  static const HighlightCountChange = 0x20;
  static const JoinStateChange = 0x40;
  static const MembersChange = 0x80;
  static const OtherChange = 0x8000;
  static const AnyChange = 0xFFFF;
}

class DefaultRoom implements Room {
  DefaultRoom(DefaultClient client, String id)
      : assert(id != null, "Room ID cannot be null"),
        this.client = client,
        this.id = id {}

  @JsonKey(ignore: true)
  DefaultClient client;

  String id;

  String prevToken;

  bool hasFullMembers = false;

  String userMembership;

  int notificationCount = 0;

  int highlightCount = 0;

  DefaultTimeline timeline = DefaultTimeline();

  List<Event> stateEvents = [];

  List<UserAccountDataFallback> accountDataEvents = [];

  List<Event> get timelineEvents =>
      timeline.tokensChunkEvents.expand((te) => te.events);

  Future<void> save() async {
    final txn = client.db.transaction(id, "readwrite");
    final store = txn.objectStore(id);
    await store.put(stateEvents.map((e) => e.toJson()).toList(), "accountData");
    await store.put(
        accountDataEvents.map((e) => e.toJson()).toList(), "accountData");
    await txn.completed;
  }

  Future<void> processRoomSync(RoomSync roomSync,
      {bool isInitialSync = false}) async {
    int changes = RoomChanges.NoChange;

    int _highlightCount = roomSync.unreadNotifications?.highlightCount ?? 0;
    int _notificationCount =
        roomSync.unreadNotifications?.notificationCount ?? 0;
    String prevToken = roomSync.timeline?.prevToken ?? "";
    bool limited = roomSync.timeline?.limited ?? false;

    processRoomSyncState(roomSync.state);
    processRoomSyncTimeline(roomSync.timeline);
    processRoomSyncAccountData(roomSync.accountData);

    if (_notificationCount != notificationCount) {
      notificationCount = _notificationCount;
    }

    if (_highlightCount != highlightCount) {
      highlightCount = _highlightCount;
    }

    if (isInitialSync) {
      // TODO: Store room back token
    }

    if (changes & RoomChanges.MembersChange > 0) {
      print("Members changed for room $id");
    }
  }

  void processRoomSyncState(RoomSyncState syncState) {
    if (syncState?.events?.isEmpty ?? true) return;

    stateEvents.addAll(syncState.events);
    for (final event in syncState.events) {}
  }

  void processRoomSyncTimeline(RoomSyncTimeline syncTimeline) {
    if (syncTimeline?.events?.isEmpty ?? true) return;

    syncTimeline.events.first.mToken =
        syncTimeline.prevToken; // Set mToken for first event.

    if (timeline.prevToken == null) {
      timeline.prevToken = syncTimeline.prevToken;
    }

    for (final event in syncTimeline.events) {
      event.roomID = id; // VERY IMPORTANT

      if (EventType.isStateEvent(event.type)) {}
    }

    final tokensChunkEvents = TokensChunkEvents()
      ..events = syncTimeline.events
      ..start = syncTimeline.prevToken;

    timeline.tokensChunkEvents.add(tokensChunkEvents);
  }

  void processBackTimelineEvents(TokensChunkEvents tokensChunkEvents) {
    if (tokensChunkEvents?.events?.isEmpty ?? true) return;

    for (final event in tokensChunkEvents.events) {}
  }

  void processRoomSyncAccountData(RoomSyncAccountData syncAccountData) {
    if (syncAccountData?.events?.isEmpty ?? true) return;

    for (final event in syncAccountData.events) {}
  }

  Future<void> store() async {}

  Future<void> postMessage(Map<String, dynamic> body) async {
    await client.api.roomAPI
        .send(this.id, "m.room.message", client.transactionID, body);
  }

  Future<void> postPlainText(String text) async {
    await postMessage({"msgtype": "m.text", "body": text});
  }

  Future<void> postEmote(String text) async {
    await postMessage({"msgtype": "m.emote", "body": text});
  }

  Future<void> getPreviousEvents(int limit) async {
    final resp = (await client.api.roomAPI
            .getMessages(this.id, prevToken, "", "b", limit, ""))
        .body;
    if (resp?.chunk?.isEmpty ?? true) return;

    timeline.prevToken = resp.end;

    final tokensChunkEvents = TokensChunkEvents()
      ..events = resp.chunk
      ..start = resp.start
      ..end = resp.end;

    tokensChunkEvents.events.last.mToken = tokensChunkEvents.end;

    await processBackTimelineEvents(tokensChunkEvents);
  }

  Future<void> fetchMembers() async {
    print("fetchMembers(): Fetching a complete list of room members...");

    final resp = (await client.api.roomAPI.getMembers(id)).body;
    if (resp?.chunk?.isEmpty ?? true) return;

    hasFullMembers = true;

    // TODO
  }
}
