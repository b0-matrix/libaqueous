import 'package:libaqueous/src/rest/events/model/tokens_chunk_events.dart';

class DefaultTimeline {
  String prevToken;

  List<TokensChunkEvents> tokensChunkEvents = [];
}
