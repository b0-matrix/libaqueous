import 'dart:async';

import 'package:chopper/chopper.dart';
import 'package:idb_shim/idb.dart' as idb;
import 'package:idb_shim/idb_io.dart' as idb_io;
import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/api/client.dart';
import 'package:libaqueous/src/internal/default_room.dart';
import 'package:libaqueous/src/rest/api.dart';
import 'package:libaqueous/src/rest/sync/model/invited_room_sync.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync.dart';
import 'package:libaqueous/src/rest/sync/model/sync_response.dart';
import 'package:libaqueous/src/rest/sync/model/user_account_data_fallback.dart';

class DefaultClient implements Client {
  DefaultClient(String homeserver, userID, token)
      : api = MatrixAPI(homeserver, token),
        id = DateTime.now().millisecondsSinceEpoch,
        userID = userID;

  String get homeserver => api.baseUrl;

  final String userID;

  String get token => api.token;

  int id;

  bool isRunning = false;

  @JsonKey(ignore: true)
  int _transactionCount = 0;

  String get transactionID =>
      id.toString() + "q" + (++_transactionCount).toString();

  @JsonKey(ignore: true)
  MatrixAPI api;

  Map<String, DefaultRoom> rooms = {};

  List<UserAccountDataFallback> accountDataEvents = [];

  String syncToken;

  idb.Database db;

  Future<void> load(Uri dbPath) async {
    idb.IdbFactory idbFactory =
    idb_io.getIdbPersistentFactory(dbPath.toString());

    db = await idbFactory.open("data.db", version: 1,
        onUpgradeNeeded: (idb.VersionChangeEvent event) {
          event.database.createObjectStore("data", autoIncrement: true);
    });

    final txn = db.transaction("data", "readonly");
    final store = txn.objectStore("data");
    syncToken = await store.getObject("syncToken") as String;
    final rawAccountDataEvents =
    await store.getObject("accountData") as List<dynamic>;
    accountDataEvents = rawAccountDataEvents
        ?.map((json) => UserAccountDataFallback.fromJson(json))
        ?.toList() ?? [];
    await txn.completed;
  }

  Future<void> save() async {
    print("Saving state...");

    final txn = db.transaction("data", "readwrite");
    final store = txn.objectStore("data");
    await store.put(syncToken, "syncToken");
    await store.put(
        accountDataEvents.map((e) => e.toJson()).toList(), "accountData");
    await txn.completed;
  }

  Future<void> startSync() async {
    if (isRunning) return;
    isRunning = true;

    await _sync();
  }

  void stopSync() {
    isRunning = false;
  }

  Future<void> logout() async {
    await api.loginAPI.postLogout();
  }

  Future<void> _sync() async {
    final stream = _getEventStream();

    await for (SyncResponse data in stream) {
      await _processSyncResponse(data);
      await save();
    }
  }

  Stream<SyncResponse> _getEventStream() async* {
    while (isRunning) {
      print("_getEventStream(): sync token: $syncToken");

      final Response<SyncResponse> resp = await api.syncAPI.sync(
          "{\"room\":{\"state\":{\"lazy_load_members\":true}}}",
          syncToken,
          syncToken?.isEmpty ?? true,
          30000);
      final syncResp = resp.body;
      syncResp.previousBatch = syncToken;

      yield syncResp;

      syncToken = syncResp.nextBatch;
    }
  }

  void _processSyncResponse(SyncResponse data) async {
    if (data == null) return;

    final isInitialSync = data.previousBatch?.isEmpty ?? true;

    // TODO: Process and store sync response.
    if (data.toDevice?.events?.isNotEmpty ?? false) {
      for (final e in data.toDevice.events) {}
    }

    if (data.accountData?.events?.isNotEmpty ?? false) {
      accountDataEvents.addAll(data.accountData.events);
    }

    if (data.rooms?.join?.isNotEmpty ?? false) {
      _processRooms(data.rooms.join, "join", isInitialSync: isInitialSync);
    }

    if (data.rooms?.leave?.isNotEmpty ?? false) {
      _processRooms(data.rooms.leave, "leave", isInitialSync: isInitialSync);
    }

    if (data.rooms?.invite?.isNotEmpty ?? false) {
      _processInvitedRooms(data.rooms.invite);
    }
  }

  void _processRooms(Map<String, RoomSync> roomSyncMap, String membership,
      {bool isInitialSync = false}) async {
    for (final id in roomSyncMap.keys) {
      final roomSync = roomSyncMap[id];

      if (!rooms.containsKey(id)) {
        rooms[id] = DefaultRoom(this, id);
      }

      final room = rooms[id];

      room.processRoomSync(roomSync, isInitialSync: isInitialSync);
    }
  }

  void _processInvitedRooms(Map<String, InvitedRoomSync> roomSyncMap) async {
    for (final id in roomSyncMap.keys) {
      final roomSync = roomSyncMap[id];

      if (!rooms.containsKey(id)) {
        rooms[id] = DefaultRoom(this, id);
      }

      final room = rooms[id];

      room.userMembership = "invite";
    }
  }
}
