// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

class _$SyncAPI extends SyncAPI {
  _$SyncAPI([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = SyncAPI;

  Future<Response<SyncResponse>> sync(
      String filter, String since, bool fullState, int timeout) {
    final $url = '/_matrix/client/r0/sync';
    final Map<String, dynamic> $params = {
      'filter': filter,
      'since': since,
      'full_state': fullState,
      'timeout': timeout
    };
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<SyncResponse, SyncResponse>($request);
  }
}
