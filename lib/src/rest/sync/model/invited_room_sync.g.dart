// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'invited_room_sync.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InvitedRoomSync _$InvitedRoomSyncFromJson(Map<String, dynamic> json) {
  return InvitedRoomSync()
    ..state = json['invite_state'] == null
        ? null
        : RoomInviteState.fromJson(
            json['invite_state'] as Map<String, dynamic>);
}

Map<String, dynamic> _$InvitedRoomSyncToJson(InvitedRoomSync instance) =>
    <String, dynamic>{'invite_state': instance.state};
