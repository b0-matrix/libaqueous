// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_account_data_direct_message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserAccountDataDirectMessages _$UserAccountDataDirectMessagesFromJson(
    Map<String, dynamic> json) {
  return UserAccountDataDirectMessages()
    ..content = (json['content'] as Map<String, dynamic>)?.map(
          (k, e) => MapEntry(k, (e as List)?.map((e) => e as String)?.toList()),
    );
}

Map<String, dynamic> _$UserAccountDataDirectMessagesToJson(
        UserAccountDataDirectMessages instance) =>
    <String, dynamic>{'content': instance.content};
