import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

part 'room_sync_timeline.g.dart';

@JsonSerializable()
class RoomSyncTimeline {
  @JsonKey(name: "events")
  List<Event> events;

  @JsonKey(name: "limited")
  bool limited;

  @JsonKey(name: "prev_batch")
  String prevToken;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomSyncTimelineFromJson;

  Map<String, dynamic> toJson() => _$RoomSyncTimelineToJson(this);
}
