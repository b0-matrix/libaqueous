import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/sync/model/user_account_data_fallback.dart';

part 'user_account_data_sync.g.dart';

@JsonSerializable()
class UserAccountDataSync {
  @JsonKey(name: "events")
  List<UserAccountDataFallback> events; // Temporary workaround

  String toString() => jsonEncode(toJson());

  static const fromJson = _$UserAccountDataSyncFromJson;

  Map<String, dynamic> toJson() => _$UserAccountDataSyncToJson(this);
}
