import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

part 'room_sync_account_data.g.dart';

@JsonSerializable()
class RoomSyncAccountData {
  @JsonKey(name: "events")
  List<Event> events;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomSyncAccountDataFromJson;

  Map<String, dynamic> toJson() => _$RoomSyncAccountDataToJson(this);
}
