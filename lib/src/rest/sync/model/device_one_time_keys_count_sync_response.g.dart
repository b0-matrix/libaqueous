// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_one_time_keys_count_sync_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeviceOneTimeKeysCountSyncResponse _$DeviceOneTimeKeysCountSyncResponseFromJson(
    Map<String, dynamic> json) {
  return DeviceOneTimeKeysCountSyncResponse()
    ..signedCurve25519 = json['signed_curve25519'] as int;
}

Map<String, dynamic> _$DeviceOneTimeKeysCountSyncResponseToJson(
        DeviceOneTimeKeysCountSyncResponse instance) =>
    <String, dynamic>{'signed_curve25519': instance.signedCurve25519};
