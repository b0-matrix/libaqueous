// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_sync_state.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomSyncState _$RoomSyncStateFromJson(Map<String, dynamic> json) {
  return RoomSyncState()
    ..events = (json['events'] as List)
        ?.map(
            (e) => e == null ? null : Event.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$RoomSyncStateToJson(RoomSyncState instance) =>
    <String, dynamic>{'events': instance.events};
