import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

part 'to_device_sync_response.g.dart';

@JsonSerializable()
class ToDeviceSyncResponse {
  @JsonKey(name: "events")
  List<Event> events;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$ToDeviceSyncResponseFromJson;

  Map<String, dynamic> toJson() => _$ToDeviceSyncResponseToJson(this);
}
