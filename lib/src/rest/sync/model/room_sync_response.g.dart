// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_sync_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomsSyncResponse _$RoomsSyncResponseFromJson(Map<String, dynamic> json) {
  return RoomsSyncResponse()
    ..join = (json['join'] as Map<String, dynamic>)?.map(
          (k, e) =>
          MapEntry(
              k,
              e == null ? null : RoomSync.fromJson(e as Map<String, dynamic>)),
    )
    ..invite = (json['invite'] as Map<String, dynamic>)?.map(
          (k, e) =>
          MapEntry(
              k,
              e == null
                  ? null
                  : InvitedRoomSync.fromJson(e as Map<String, dynamic>)),
    )
    ..leave = (json['leave'] as Map<String, dynamic>)?.map(
          (k, e) =>
          MapEntry(
              k,
              e == null ? null : RoomSync.fromJson(e as Map<String, dynamic>)),
    );
}

Map<String, dynamic> _$RoomsSyncResponseToJson(RoomsSyncResponse instance) =>
    <String, dynamic>{
      'join': instance.join,
      'invite': instance.invite,
      'leave': instance.leave
    };
