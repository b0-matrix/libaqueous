// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_sync_timeline.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomSyncTimeline _$RoomSyncTimelineFromJson(Map<String, dynamic> json) {
  return RoomSyncTimeline()
    ..events = (json['events'] as List)
        ?.map(
            (e) => e == null ? null : Event.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..limited = json['limited'] as bool
    ..prevToken = json['prev_batch'] as String;
}

Map<String, dynamic> _$RoomSyncTimelineToJson(RoomSyncTimeline instance) =>
    <String, dynamic>{
      'events': instance.events,
      'limited': instance.limited,
      'prev_batch': instance.prevToken
    };
