// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_data_element.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountDataElement _$AccountDataElementFromJson(Map<String, dynamic> json) {
  return AccountDataElement()
    ..type = json['type'] as String
    ..content = json['content'] as Map<String, dynamic>;
}

Map<String, dynamic> _$AccountDataElementToJson(AccountDataElement instance) =>
    <String, dynamic>{'type': instance.type, 'content': instance.content};
