import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync_account_data.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync_ephemeral.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync_state.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync_summary.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync_timeline.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync_unread_notifications.dart';

part 'room_sync.g.dart';

@JsonSerializable()
class RoomSync {
  @JsonKey(name: "state")
  RoomSyncState state;

  @JsonKey(name: "timeline")
  RoomSyncTimeline timeline;

  @JsonKey(name: "ephemeral")
  RoomSyncEphemeral ephemeral;

  @JsonKey(name: "account_data")
  RoomSyncAccountData accountData;

  @JsonKey(name: "unread_notifications")
  RoomSyncUnreadNotifications unreadNotifications;

  @JsonKey(name: "summary")
  RoomSyncSummary summary;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomSyncFromJson;

  Map<String, dynamic> toJson() => _$RoomSyncToJson(this);
}
