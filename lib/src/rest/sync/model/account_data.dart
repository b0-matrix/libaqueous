import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/sync/model/account_data_element.dart';

part 'account_data.g.dart';

@JsonSerializable()
class AccountData {
  @JsonKey(name: "events")
  List<AccountDataElement> accountDataElements;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$AccountDataFromJson;

  Map<String, dynamic> toJson() => _$AccountDataToJson(this);
}
