import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/sync/model/room_invite_state.dart';

part 'invited_room_sync.g.dart';

@JsonSerializable()
class InvitedRoomSync {
  @JsonKey(name: "invite_state")
  RoomInviteState state;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$InvitedRoomSyncFromJson;

  Map<String, dynamic> toJson() => _$InvitedRoomSyncToJson(this);
}
