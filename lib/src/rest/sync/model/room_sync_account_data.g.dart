// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_sync_account_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomSyncAccountData _$RoomSyncAccountDataFromJson(Map<String, dynamic> json) {
  return RoomSyncAccountData()
    ..events = (json['events'] as List)
        ?.map(
            (e) => e == null ? null : Event.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$RoomSyncAccountDataToJson(
        RoomSyncAccountData instance) =>
    <String, dynamic>{'events': instance.events};
