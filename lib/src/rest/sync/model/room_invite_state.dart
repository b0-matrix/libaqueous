import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

part 'room_invite_state.g.dart';

@JsonSerializable()
class RoomInviteState {
  @JsonKey(name: "events")
  List<Event> events;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomInviteStateFromJson;

  Map<String, dynamic> toJson() => _$RoomInviteStateToJson(this);
}
