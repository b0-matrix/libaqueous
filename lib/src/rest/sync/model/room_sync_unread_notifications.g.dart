// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_sync_unread_notifications.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomSyncUnreadNotifications _$RoomSyncUnreadNotificationsFromJson(
    Map<String, dynamic> json) {
  return RoomSyncUnreadNotifications()
    ..events = (json['events'] as List)
        ?.map(
            (e) => e == null ? null : Event.fromJson(e as Map<String, dynamic>))
        ?.toList()
    ..notificationCount = json['notification_count'] as int
    ..highlightCount = json['highlight_count'] as int;
}

Map<String, dynamic> _$RoomSyncUnreadNotificationsToJson(
        RoomSyncUnreadNotifications instance) =>
    <String, dynamic>{
      'events': instance.events,
      'notification_count': instance.notificationCount,
      'highlight_count': instance.highlightCount
    };
