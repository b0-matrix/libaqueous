import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'device_info.g.dart';

@JsonSerializable()
class DeviceInfo {
  @JsonKey(name: "user_id")
  String userID;

  @JsonKey(name: "device_id")
  String deviceID;

  @JsonKey(name: "display_name")
  String displayName;

  @JsonKey(name: "last_seen_ts")
  int lastSeenTs;

  @JsonKey(name: "last_seen_ip")
  String lastSeenIP;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$DeviceInfoFromJson;

  Map<String, dynamic> toJson() => _$DeviceInfoToJson(this);
}
