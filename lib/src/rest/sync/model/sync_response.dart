import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/sync/model/device_list_response.dart';
import 'package:libaqueous/src/rest/sync/model/device_one_time_keys_count_sync_response.dart';
import 'package:libaqueous/src/rest/sync/model/presence_sync_response.dart';
import 'package:libaqueous/src/rest/sync/model/room_sync_response.dart';
import 'package:libaqueous/src/rest/sync/model/to_device_sync_response.dart';
import 'package:libaqueous/src/rest/sync/model/user_account_data_sync.dart';

part 'sync_response.g.dart';

@JsonSerializable()
class SyncResponse {
  @JsonKey(name: "account_data")
  UserAccountDataSync accountData;

  @JsonKey(ignore: true)
  String previousBatch;

  @JsonKey(name: "next_batch")
  String nextBatch;

  @JsonKey(name: "presence")
  PresenceSyncResponse presence;

  @JsonKey(name: "to_device")
  ToDeviceSyncResponse toDevice;

  @JsonKey(name: "rooms")
  RoomsSyncResponse rooms;

  @JsonKey(name: "device_lists")
  DeviceListResponse deviceLists;

  @JsonKey(name: "device_one_time_keys_count")
  DeviceOneTimeKeysCountSyncResponse deviceOneTimeKeysCount;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$SyncResponseFromJson;

  Map<String, dynamic> toJson() => _$SyncResponseToJson(this);
}
