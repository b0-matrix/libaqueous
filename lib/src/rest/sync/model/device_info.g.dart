// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeviceInfo _$DeviceInfoFromJson(Map<String, dynamic> json) {
  return DeviceInfo()
    ..userID = json['user_id'] as String
    ..deviceID = json['device_id'] as String
    ..displayName = json['display_name'] as String
    ..lastSeenTs = json['last_seen_ts'] as int
    ..lastSeenIP = json['last_seen_ip'] as String;
}

Map<String, dynamic> _$DeviceInfoToJson(DeviceInfo instance) =>
    <String, dynamic>{
      'user_id': instance.userID,
      'device_id': instance.deviceID,
      'display_name': instance.displayName,
      'last_seen_ts': instance.lastSeenTs,
      'last_seen_ip': instance.lastSeenIP
    };
