import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'device_list_response.g.dart';

@JsonSerializable()
class DeviceListResponse {
  @JsonKey(name: "changed")
  List<String> changed;

  @JsonKey(name: "left")
  List<String> left;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$DeviceListResponseFromJson;

  Map<String, dynamic> toJson() => _$DeviceListResponseToJson(this);
}
