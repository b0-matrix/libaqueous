// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'matrix_error.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MatrixError _$MatrixErrorFromJson(Map<String, dynamic> json) {
  return MatrixError(
      code: _$enumDecodeNullable(_$MatrixErrorCodeEnumMap, json['errcode']),
      error: json['error'] as String);
}

Map<String, dynamic> _$MatrixErrorToJson(MatrixError instance) =>
    <String, dynamic>{
      'errcode': _$MatrixErrorCodeEnumMap[instance.code],
      'error': instance.error
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$MatrixErrorCodeEnumMap = <MatrixErrorCode, dynamic>{
  MatrixErrorCode.M_FORBIDDEN: 'M_FORBIDDEN',
  MatrixErrorCode.M_UNKNOWN_TOKEN: 'M_UNKNOWN_TOKEN',
  MatrixErrorCode.M_MISSING_TOKEN: 'M_MISSING_TOKEN',
  MatrixErrorCode.M_BAD_JSON: 'M_BAD_JSON',
  MatrixErrorCode.M_NOT_JSON: 'M_NOT_JSON',
  MatrixErrorCode.M_NOT_FOUND: 'M_NOT_FOUND',
  MatrixErrorCode.M_LIMIT_EXCEEDED: 'M_LIMIT_EXCEEDED',
  MatrixErrorCode.M_USER_IN_USE: 'M_USER_IN_USE',
  MatrixErrorCode.M_ROOM_IN_USE: 'M_ROOM_IN_USE',
  MatrixErrorCode.M_BAD_PAGINATION: 'M_BAD_PAGINATION',
  MatrixErrorCode.M_EXCLUSIVE: 'M_EXCLUSIVE',
  MatrixErrorCode.M_UNKNOWN: 'M_UNKNOWN',
  MatrixErrorCode.M_TOO_LARGE: 'M_TOO_LARGE',
  MatrixErrorCode.M_UNRECOGNIZED: 'M_UNRECOGNIZED',
  MatrixErrorCode.CL_UNKNOWN_ERROR_CODE: 'CL_UNKNOWN_ERROR_CODE',
  MatrixErrorCode.CL_NONE: 'CL_NONE'
};
