import 'package:chopper/chopper.dart';
import 'package:libaqueous/src/rest/converter.dart';
import 'package:libaqueous/src/rest/login/login.dart';
import 'package:libaqueous/src/rest/room/model/created_event.dart';
import 'package:libaqueous/src/rest/room/model/joined_rooms.dart';
import 'package:libaqueous/src/rest/room/model/room_alias.dart';
import 'package:libaqueous/src/rest/room/model/room_id.dart';
import 'package:libaqueous/src/rest/room/model/room_members.dart';
import 'package:libaqueous/src/rest/room/model/room_messages.dart';
import 'package:libaqueous/src/rest/room/room.dart';
import 'package:libaqueous/src/rest/sync/model/sync_response.dart';
import 'package:libaqueous/src/rest/sync/sync.dart';

class MatrixAPI {
  final ChopperClient chopper;

  final LoginAPI loginAPI;
  final SyncAPI syncAPI;
  final RoomAPI roomAPI;

  final String baseUrl;
  final String token;

  MatrixAPI._fromAPI(this.chopper, this.loginAPI, this.syncAPI, this.roomAPI,
      this.baseUrl, this.token);

  factory MatrixAPI(String baseUrl, String token) {
    final converter = JsonSerializableConverter({
      JoinedRooms: JoinedRooms.fromJson,
      RoomID: RoomID.fromJson,
      RoomAlias: RoomAlias.fromJson,
      SyncResponse: SyncResponse.fromJson,
      CreatedEvent: CreatedEvent.fromJson,
      RoomMessages: RoomMessages.fromJson,
      RoomMembers: RoomMembers.fromJson,
    });

    final _chopper = ChopperClient(
      baseUrl: baseUrl,
      services: [
        LoginAPI.create(),
        SyncAPI.create(),
        RoomAPI.create(),
      ],
      converter: converter,
      errorConverter: converter,
      interceptors: [
        (Request request) async =>
            request.replace(headers: {"Authorization": "Bearer " + token}),
      ],
    );

    final _loginAPI = _chopper.getService<LoginAPI>();
    final _syncAPI = _chopper.getService<SyncAPI>();
    final _roomAPI = _chopper.getService<RoomAPI>();

    return MatrixAPI._fromAPI(
        _chopper, _loginAPI, _syncAPI, _roomAPI, baseUrl, token);
  }
}
