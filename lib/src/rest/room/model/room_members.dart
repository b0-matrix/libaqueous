import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/room_member_event.dart';

part 'room_members.g.dart';

@JsonSerializable()
class RoomMembers {
  List<RoomMemberEvent> chunk;

  static const fromJson = _$RoomMembersFromJson;

  Map<String, dynamic> toJson() => _$RoomMembersToJson(this);
}
