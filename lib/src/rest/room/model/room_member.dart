import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_member.g.dart';

@JsonSerializable()
class RoomMember {
  @JsonKey(name: "membership")
  String membership;

  @JsonKey(name: "displayname")
  String displayName;

  @JsonKey(name: "avatar_url")
  String avatarUrl;

  @JsonKey(name: "is_direct")
  bool isDirect = false;

  @JsonKey(name: "unsigned")
  UnsignedData unsignedData;

  static const fromJson = _$RoomMemberFromJson;

  Map<String, dynamic> toJson() => _$RoomMemberToJson(this);
}
