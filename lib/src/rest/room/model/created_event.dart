import 'package:json_annotation/json_annotation.dart';

part 'created_event.g.dart';

@JsonSerializable()
class CreatedEvent {
  @JsonKey(name: "event_id")
  String eventID;

  static const fromJson = _$CreatedEventFromJson;

  Map<String, dynamic> toJson() => _$CreatedEventToJson(this);
}
