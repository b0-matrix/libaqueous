// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_alias.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomAlias _$RoomAliasFromJson(Map<String, dynamic> json) {
  return RoomAlias()
    ..roomID = json['room_id'] as String
    ..servers = (json['servers'] as List)?.map((e) => e as String)?.toList();
}

Map<String, dynamic> _$RoomAliasToJson(RoomAlias instance) =>
    <String, dynamic>{'room_id': instance.roomID, 'servers': instance.servers};
