// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'joined_rooms.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JoinedRooms _$JoinedRoomsFromJson(Map<String, dynamic> json) {
  return JoinedRooms()
    ..joinedRooms =
        (json['joined_rooms'] as List)?.map((e) => e as String)?.toList();
}

Map<String, dynamic> _$JoinedRoomsToJson(JoinedRooms instance) =>
    <String, dynamic>{'joined_rooms': instance.joinedRooms};
