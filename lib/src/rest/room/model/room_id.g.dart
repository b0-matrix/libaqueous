// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_id.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomID _$RoomIDFromJson(Map<String, dynamic> json) {
  return RoomID()
    ..roomID = json['room_id'] as String;
}

Map<String, dynamic> _$RoomIDToJson(RoomID instance) =>
    <String, dynamic>{'room_id': instance.roomID};
