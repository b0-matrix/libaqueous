import 'package:json_annotation/json_annotation.dart';

part 'room_alias.g.dart';

@JsonSerializable()
class RoomAlias {
  @JsonKey(name: "room_id")
  String roomID;

  @JsonKey(name: "servers")
  List<String> servers;

  static const fromJson = _$RoomAliasFromJson;

  Map<String, dynamic> toJson() => _$RoomAliasToJson(this);
}
