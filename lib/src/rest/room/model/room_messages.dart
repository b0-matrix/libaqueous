import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

part 'room_messages.g.dart';

@JsonSerializable()
class RoomMessages {
  @JsonKey(name: "start")
  String start;

  @JsonKey(name: "end")
  String end;

  @JsonKey(name: "chunk")
  List<Event> chunk;

  static const fromJson = _$RoomMessagesFromJson;

  Map<String, dynamic> toJson() => _$RoomMessagesToJson(this);
}
