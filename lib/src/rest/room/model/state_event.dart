import 'package:json_annotation/json_annotation.dart';

part 'state_event.g.dart';

@JsonSerializable()
class StateEvent {
  @JsonKey(name: "type", nullable: false)
  String type;

  @JsonKey(name: "state_key")
  String stateKey;

  @JsonKey(name: "content")
  Map<String, dynamic> content;

  static const fromJson = _$StateEventFromJson;

  Map<String, dynamic> toJson() => _$StateEventToJson(this);
}
