// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_messages.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomMessages _$RoomMessagesFromJson(Map<String, dynamic> json) {
  return RoomMessages()
    ..start = json['start'] as String
    ..end = json['end'] as String
    ..chunk = (json['chunk'] as List)
        ?.map(
            (e) => e == null ? null : Event.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$RoomMessagesToJson(RoomMessages instance) =>
    <String, dynamic>{
      'start': instance.start,
      'end': instance.end,
      'chunk': instance.chunk
    };
