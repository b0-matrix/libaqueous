// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'state_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StateEvent _$StateEventFromJson(Map<String, dynamic> json) {
  return StateEvent()
    ..type = json['type'] as String
    ..stateKey = json['state_key'] as String
    ..content = json['content'] as Map<String, dynamic>;
}

Map<String, dynamic> _$StateEventToJson(StateEvent instance) =>
    <String, dynamic>{
      'type': instance.type,
      'state_key': instance.stateKey,
      'content': instance.content
    };
