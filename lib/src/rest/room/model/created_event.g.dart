// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'created_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreatedEvent _$CreatedEventFromJson(Map<String, dynamic> json) {
  return CreatedEvent()..eventID = json['event_id'] as String;
}

Map<String, dynamic> _$CreatedEventToJson(CreatedEvent instance) =>
    <String, dynamic>{'event_id': instance.eventID};
