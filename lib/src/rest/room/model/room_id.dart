import 'package:json_annotation/json_annotation.dart';

part 'room_id.g.dart';

@JsonSerializable()
class RoomID {
  @JsonKey(name: "room_id")
  String roomID;

  static const fromJson = _$RoomIDFromJson;

  Map<String, dynamic> toJson() => _$RoomIDToJson(this);
}
