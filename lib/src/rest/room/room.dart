import "dart:async";

import 'package:chopper/chopper.dart';
import 'package:libaqueous/src/rest/const.dart';
import 'package:libaqueous/src/rest/room/model/created_event.dart';
import 'package:libaqueous/src/rest/room/model/directory_visibility.dart';
import 'package:libaqueous/src/rest/room/model/joined_rooms.dart';
import 'package:libaqueous/src/rest/room/model/room_id.dart';
import 'package:libaqueous/src/rest/room/model/room_members.dart';
import 'package:libaqueous/src/rest/room/model/room_messages.dart';
import 'package:libaqueous/src/rest/room/model/state_event.dart';

part "room.chopper.dart";

@ChopperApi(baseUrl: MATRIX_CLIENT_R0)
abstract class RoomAPI extends ChopperService {
  static RoomAPI create([ChopperClient client]) => _$RoomAPI(client);

  @Post(path: "/createRoom")
  Future<Response<RoomID>> postCreateRoom(
      @Query("visibility") DirectoryVisibility visibility,
      @Query("room_alias_name") String aliasName,
      @Query("name") String name,
      @Query("topic") String topic,
      @Query("invite") List<String> invite,
      @Query("room_version") String version,
      @Query("initial_state") List<StateEvent> initialState,
      @Query("is_direct") bool isDirect);

  @Put(path: "/directory/room/{roomAlias}")
  Future<Response> putRoomAlias(
      @Path("roomAlias") String roomAlias, @Query("room_id") String roomID);

  @Get(path: "/directory/room/{roomAlias}")
  Future<Response> getRoomAlias(@Path("roomAlias") String roomAlias);

  @Delete(path: "/directory/room/{roomAlias}")
  Future<Response> deleteRoomAlias(@Path("roomAlias") String roomAlias);

  @Get(path: "/joined_rooms")
  Future<Response<JoinedRooms>> getJoinedRooms();

  @Post(path: "/rooms/{roomID}/invite")
  Future<Response> postInvite(
      @Path("roomID") String roomID, @Query("user_id") String userID);

  @Post(path: "/rooms/{roomID}/join")
  Future<Response> postJoin(@Path("roomID") String roomID);

  @Put(path: "/rooms/{roomID}/send/{eventType}/{txID}")
  Future<Response<CreatedEvent>> send(
      @Path("roomID") String roomID,
      @Path("eventType") String eventType,
      @Path("txID") String txID,
      @Body() content);

  @Get(path: "/rooms/{roomID}/messages")
  Future<Response<RoomMessages>> getMessages(
    @Path("roomID") String roomID,
    @Query("from") String from,
    @Query("to") String to,
    @Query("dir") String dir,
    @Query("limit") int limit,
    @Query("filter") String filter,
  );

  @Get(path: "/rooms/{roomID}/members")
  Future<Response<RoomMembers>> getMembers(@Path("roomID") String roomID);
}
