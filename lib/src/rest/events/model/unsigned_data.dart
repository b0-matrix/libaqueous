import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

part 'unsigned_data.g.dart';

@JsonSerializable()
class UnsignedData {
  @JsonKey(name: "age")
  int age;

  @JsonKey(name: "redacted_because")
  Event redactedEvent;

  @JsonKey(name: "transaction_id")
  String transactionID;

  @JsonKey(name: "prev_content")
  Map<String, dynamic> prevContent;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$UnsignedDataFromJson;

  Map<String, dynamic> toJson() => _$UnsignedDataToJson(this);
}
