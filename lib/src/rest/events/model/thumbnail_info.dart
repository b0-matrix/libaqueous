import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/image_info.dart';

part 'thumbnail_info.g.dart';

@JsonSerializable()
class ThumbnailInfo extends ImageInfo {
  static const fromJson = _$ThumbnailInfoFromJson;

  Map<String, dynamic> toJson() => _$ThumbnailInfoToJson(this);
}
