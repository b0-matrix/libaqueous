// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tokens_chunk_events.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TokensChunkEvents _$TokensChunkEventsFromJson(Map<String, dynamic> json) {
  return TokensChunkEvents()
    ..start = json['start'] as String
    ..end = json['end'] as String
    ..events = (json['events'] as List)
        ?.map(
            (e) => e == null ? null : Event.fromJson(e as Map<String, dynamic>))
        ?.toList();
}

Map<String, dynamic> _$TokensChunkEventsToJson(TokensChunkEvents instance) =>
    <String, dynamic>{
      'start': instance.start,
      'end': instance.end,
      'events': instance.events
    };
