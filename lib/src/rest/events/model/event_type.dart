class EventType {
  static const String PRESENCE = "m.presence";
  static const String MESSAGE = "m.room.message";
  static const String STICKER = "m.sticker";
  static const String ENCRYPTED = "m.room.encrypted";
  static const String ENCRYPTION = "m.room.encryption";
  static const String FEEDBACK = "m.room.message.feedback";
  static const String TYPING = "m.typing";
  static const String REDACTION = "m.room.redaction";
  static const String RECEIPT = "m.receipt";
  static const String TAG = "m.tag";
  static const String ROOM_KEY = "m.room_key";
  static const String FULLY_READ = "m.fully_read";
  static const String PLUMBING = "m.room.plumbing";
  static const String BOT_OPTIONS = "m.room.bot.options";
  static const String KEY_REQUEST = "m.room_key_request";
  static const String FORWARDED_ROOM_KEY = "m.forwarded_room_key";
  static const String PREVIEW_URLS = "org.matrix.room.preview_urls";

  static const String STATE_ROOM_NAME = "m.room.name";
  static const String STATE_ROOM_TOPIC = "m.room.topic";
  static const String STATE_ROOM_AVATAR = "m.room.avatar";
  static const String STATE_ROOM_MEMBER = "m.room.member";
  static const String STATE_ROOM_THIRD_PARTY_INVITE =
      "m.room.third_party_invite";
  static const String STATE_ROOM_CREATE = "m.room.create";
  static const String STATE_ROOM_JOIN_RULES = "m.room.join_rules";
  static const String STATE_ROOM_GUEST_ACCESS = "m.room.guest_access";
  static const String STATE_ROOM_POWER_LEVELS = "m.room.power_levels";
  static const String STATE_ROOM_ALIASES = "m.room.aliases";
  static const String STATE_ROOM_TOMBSTONE = "m.room.tombstone";
  static const String STATE_CANONICAL_ALIAS = "m.room.canonical_alias";
  static const String STATE_HISTORY_VISIBILITY = "m.room.history_visibility";
  static const String STATE_RELATED_GROUPS = "m.room.related_groups";
  static const String STATE_PINNED_EVENT = "m.room.pinned_events";

  static const String CALL_INVITE = "m.call.invite";
  static const String CALL_CANDIDATES = "m.call.candidates";
  static const String CALL_ANSWER = "m.call.answer";
  static const String CALL_HANGUP = "m.call.hangup";

  static const List<String> STATE_EVENTS = [
    STATE_ROOM_NAME,
    STATE_ROOM_TOPIC,
    STATE_ROOM_AVATAR,
    STATE_ROOM_MEMBER,
    STATE_ROOM_THIRD_PARTY_INVITE,
    STATE_ROOM_CREATE,
    STATE_ROOM_JOIN_RULES,
    STATE_ROOM_GUEST_ACCESS,
    STATE_ROOM_POWER_LEVELS,
    STATE_ROOM_TOMBSTONE,
    STATE_HISTORY_VISIBILITY,
    STATE_RELATED_GROUPS,
    STATE_PINNED_EVENT
  ];

  static bool isStateEvent(String type) => STATE_EVENTS.contains(type);
}
