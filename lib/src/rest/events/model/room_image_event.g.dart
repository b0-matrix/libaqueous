// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_image_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomImageEventContent _$RoomImageEventContentFromJson(
    Map<String, dynamic> json) {
  return RoomImageEventContent()
    ..rawContent = json['rawContent'] as Map<String, dynamic>
    ..body = json['body'] as String
    ..type = _$enumDecodeNullable(_$MessageEventTypeEnumMap, json['msgtype'])
    ..info = json['info'] == null
        ? null
        : ImageInfo.fromJson(json['info'] as Map<String, dynamic>)
    ..url = json['url'] == null ? null : Uri.parse(json['url'] as String);
}

Map<String, dynamic> _$RoomImageEventContentToJson(
        RoomImageEventContent instance) =>
    <String, dynamic>{
      'rawContent': instance.rawContent,
      'body': instance.body,
      'msgtype': _$MessageEventTypeEnumMap[instance.type],
      'info': instance.info,
      'url': instance.url?.toString()
    };

T _$enumDecode<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    throw ArgumentError('A value must be provided. Supported values: '
        '${enumValues.values.join(', ')}');
  }
  return enumValues.entries
      .singleWhere((e) => e.value == source,
          orElse: () => throw ArgumentError(
              '`$source` is not one of the supported values: '
              '${enumValues.values.join(', ')}'))
      .key;
}

T _$enumDecodeNullable<T>(Map<T, dynamic> enumValues, dynamic source) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<T>(enumValues, source);
}

const _$MessageEventTypeEnumMap = <MessageEventType, dynamic>{
  MessageEventType.TEXT: 'm.text',
  MessageEventType.EMOTE: 'm.emote',
  MessageEventType.NOTICE: 'm.notice',
  MessageEventType.IMAGE: 'm.image',
  MessageEventType.FILE: 'm.file',
  MessageEventType.VIDEO: 'm.video',
  MessageEventType.AUDIO: 'm.audio',
  MessageEventType.LOCATION: 'm.location'
};

RoomImageEvent _$RoomImageEventFromJson(Map<String, dynamic> json) {
  return RoomImageEvent()
    ..type = json['type'] as String
    ..eventID = json['event_id'] as String
    ..refreshToken = json['prev_content'] as Map<String, dynamic>
    ..originServerTs = json['origin_server_ts'] as int
    ..sender = json['sender'] as String
    ..stateKey = json['state_key'] as String
    ..roomID = json['room_id'] as String
    ..unsignedData = json['unsigned'] == null
        ? null
        : UnsignedData.fromJson(json['unsigned'] as Map<String, dynamic>)
    ..redacts = json['redacts'] as String
    ..content = json['content'] == null
        ? null
        : RoomImageEventContent.fromJson(
            json['content'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RoomImageEventToJson(RoomImageEvent instance) =>
    <String, dynamic>{
      'type': instance.type,
      'event_id': instance.eventID,
      'prev_content': instance.refreshToken,
      'origin_server_ts': instance.originServerTs,
      'sender': instance.sender,
      'state_key': instance.stateKey,
      'room_id': instance.roomID,
      'unsigned': instance.unsignedData,
      'redacts': instance.redacts,
      'content': instance.content
    };
