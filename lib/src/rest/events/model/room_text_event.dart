import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/room_message_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_text_event.g.dart';

@JsonSerializable()
class RoomTextEvent extends RoomMessageEvent {
  @JsonKey(name: "content")
  covariant RoomMessageEventContent content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomTextEventFromJson;

  Map<String, dynamic> toJson() => _$RoomTextEventToJson(this);
}
