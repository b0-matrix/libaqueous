class RoomTag {
  static const String ROOM_TAG_FAVOURITE = "m.favourite";
  static const String ROOM_TAG_LOW_PRIORITY = "m.lowpriority";
  static const String ROOM_TAG_NO_TAG = "m.recent";
  static const String ROOM_TAG_SERVER_NOTICE = "m.server_notice";

  String name;

  double order;
}
