import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/room_state_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_topic_event.g.dart';

@JsonSerializable()
class RoomTopicEventContent extends EventContent {
  @JsonKey(name: "topic")
  String topic;

  static EventContent fromJson(Map<String, dynamic> json) {
    return _$RoomTopicEventContentFromJson(json)
      ..rawContent = json;
  }
}

@JsonSerializable()
class RoomTopicEvent extends RoomStateEvent {
  @JsonKey(name: "content")
  covariant RoomTopicEventContent content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomTopicEventFromJson;

  Map<String, dynamic> toJson() => _$RoomTopicEventToJson(this);
}
