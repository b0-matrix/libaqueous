import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/room_message_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_notice_event.g.dart';

@JsonSerializable()
class RoomNoticeEvent extends RoomMessageEvent {
  @JsonKey(name: "content")
  covariant RoomMessageEventContent content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomNoticeEventFromJson;

  Map<String, dynamic> toJson() => _$RoomNoticeEventToJson(this);
}
