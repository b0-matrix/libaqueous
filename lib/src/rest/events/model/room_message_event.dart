import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/room_emote_event.dart';
import 'package:libaqueous/src/rest/events/model/room_event.dart';
import 'package:libaqueous/src/rest/events/model/room_image_event.dart';
import 'package:libaqueous/src/rest/events/model/room_notice_event.dart';
import 'package:libaqueous/src/rest/events/model/room_text_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_message_event.g.dart';

enum MessageEventType {
@JsonValue("m.text")
TEXT,@JsonValue("m.emote")
EMOTE,@JsonValue("m.notice")
NOTICE,@JsonValue("m.image")
IMAGE,@JsonValue("m.file")
FILE,@JsonValue("m.video")
VIDEO,@JsonValue("m.audio")
AUDIO,@JsonValue("m.location")
LOCATION,}

@JsonSerializable()
class RoomRichMessageEventContent extends RoomMessageEventContent {
  @JsonKey(name: "format")
  String format;

  @JsonKey(name: "formatted_body")
  String formattedBody;

  static EventContent fromJson(Map<String, dynamic> json) {
    return _$RoomRichMessageEventContentFromJson(json)
      ..rawContent = json;
  }
}

@JsonSerializable()
class RoomMessageEventContent extends EventContent {
  @JsonKey(name: "body")
  String body;

  @JsonKey(name: "msgtype")
  MessageEventType type;

  static EventContent fromJson(Map<String, dynamic> json) {
    if (json.containsKey("format") || json.containsKey("formatted_body")) {
      return RoomRichMessageEventContent.fromJson(json);
    }
    return _$RoomMessageEventContentFromJson(json)
      ..rawContent = json;
  }
}

@JsonSerializable()
class RoomMessageEvent extends RoomEvent {
  @JsonKey(name: "content")
  covariant RoomMessageEventContent content;

  String toString() => jsonEncode(toJson());

  static RoomMessageEvent fromJson(Map<String, dynamic> json) {
    switch (json["content"]["msgtype"] ?? "") {
      case "m.text":
        return RoomTextEvent.fromJson(json);
      case "m.emote":
        return RoomEmoteEvent.fromJson(json);
      case "m.notice":
        return RoomNoticeEvent.fromJson(json);
      case "m.image":
        return RoomImageEvent.fromJson(json);
    }
    return _$RoomMessageEventFromJson(json);
  }

  Map<String, dynamic> toJson() => _$RoomMessageEventToJson(this);
}
