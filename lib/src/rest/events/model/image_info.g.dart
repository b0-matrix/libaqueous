// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'image_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ImageInfo _$ImageInfoFromJson(Map<String, dynamic> json) {
  return ImageInfo()
    ..mimeType = json['mimetype'] as String
    ..size = json['size'] as int
    ..thumbnailUrl = json['thumbnail_url'] == null
        ? null
        : Uri.parse(json['thumbnail_url'] as String)
    ..thumbnailInfo = json['thumbnail_info'] == null
        ? null
        : ThumbnailInfo.fromJson(json['thumbnail_info'] as Map<String, dynamic>)
    ..width = json['w'] as int
    ..height = json['h'] as int;
}

Map<String, dynamic> _$ImageInfoToJson(ImageInfo instance) => <String, dynamic>{
      'mimetype': instance.mimeType,
      'size': instance.size,
      'thumbnail_url': instance.thumbnailUrl?.toString(),
      'thumbnail_info': instance.thumbnailInfo,
      'w': instance.width,
      'h': instance.height
    };
