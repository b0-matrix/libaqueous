// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_topic_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomTopicEventContent _$RoomTopicEventContentFromJson(
    Map<String, dynamic> json) {
  return RoomTopicEventContent()
    ..rawContent = json['rawContent'] as Map<String, dynamic>
    ..topic = json['topic'] as String;
}

Map<String, dynamic> _$RoomTopicEventContentToJson(
    RoomTopicEventContent instance) =>
    <String, dynamic>{
      'rawContent': instance.rawContent,
      'topic': instance.topic
    };

RoomTopicEvent _$RoomTopicEventFromJson(Map<String, dynamic> json) {
  return RoomTopicEvent()
    ..type = json['type'] as String
    ..eventID = json['event_id'] as String
    ..refreshToken = json['prev_content'] as Map<String, dynamic>
    ..originServerTs = json['origin_server_ts'] as int
    ..sender = json['sender'] as String
    ..stateKey = json['state_key'] as String
    ..roomID = json['room_id'] as String
    ..unsignedData = json['unsigned'] == null
        ? null
        : UnsignedData.fromJson(json['unsigned'] as Map<String, dynamic>)
    ..redacts = json['redacts'] as String
    ..content = json['content'] == null
        ? null
        : RoomTopicEventContent.fromJson(
        json['content'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RoomTopicEventToJson(RoomTopicEvent instance) =>
    <String, dynamic>{
      'type': instance.type,
      'event_id': instance.eventID,
      'prev_content': instance.refreshToken,
      'origin_server_ts': instance.originServerTs,
      'sender': instance.sender,
      'state_key': instance.stateKey,
      'room_id': instance.roomID,
      'unsigned': instance.unsignedData,
      'redacts': instance.redacts,
      'content': instance.content
    };
