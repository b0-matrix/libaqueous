import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/room_state_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_canonical_alias_event.g.dart';

@JsonSerializable()
class RoomCanonicalAliasEventContent extends EventContent {
  @JsonKey(name: "alias")
  String alias;

  static EventContent fromJson(Map<String, dynamic> json) {
    return _$RoomCanonicalAliasEventContentFromJson(json)..rawContent = json;
  }
}

@JsonSerializable()
class RoomCanonicalAliasEvent extends RoomStateEvent {
  @JsonKey(name: "content")
  covariant RoomCanonicalAliasEventContent content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomCanonicalAliasEventFromJson;

  Map<String, dynamic> toJson() => _$RoomCanonicalAliasEventToJson(this);
}
