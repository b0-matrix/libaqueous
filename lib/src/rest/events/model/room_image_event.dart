import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/image_info.dart';
import 'package:libaqueous/src/rest/events/model/room_message_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_image_event.g.dart';

@JsonSerializable()
class RoomImageEventContent extends RoomMessageEventContent {
  @JsonKey(name: "info")
  ImageInfo info;

  @JsonKey(name: "url")
  Uri url;

  static EventContent fromJson(Map<String, dynamic> json) {
    return _$RoomImageEventContentFromJson(json)..rawContent = json;
  }
}

@JsonSerializable()
class RoomImageEvent extends RoomMessageEvent {
  @JsonKey(name: "content")
  covariant RoomImageEventContent content;

  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomImageEventFromJson;

  Map<String, dynamic> toJson() => _$RoomImageEventToJson(this);
}
