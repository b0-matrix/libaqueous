import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/file_info.dart';
import 'package:libaqueous/src/rest/events/model/thumbnail_info.dart';

part 'image_info.g.dart';

@JsonSerializable()
class ImageInfo extends FileInfo {
  @JsonKey(name: "w")
  int width;

  @JsonKey(name: "h")
  int height;

  static const fromJson = _$ImageInfoFromJson;

  Map<String, dynamic> toJson() => _$ImageInfoToJson(this);
}
