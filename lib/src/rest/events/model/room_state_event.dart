import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';
import 'package:libaqueous/src/rest/events/model/room_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'room_state_event.g.dart';

@JsonSerializable()
class RoomStateEvent extends RoomEvent {
  String toString() => jsonEncode(toJson());

  static const fromJson = _$RoomStateEventFromJson;

  Map<String, dynamic> toJson() => _$RoomStateEventToJson(this);
}
