// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_avatar_event.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RoomAvatarEventContent _$RoomAvatarEventContentFromJson(
    Map<String, dynamic> json) {
  return RoomAvatarEventContent()
    ..rawContent = json['rawContent'] as Map<String, dynamic>
    ..info = json['info'] == null
        ? null
        : ImageInfo.fromJson(json['info'] as Map<String, dynamic>)
    ..url = json['url'] == null ? null : Uri.parse(json['url'] as String);
}

Map<String, dynamic> _$RoomAvatarEventContentToJson(
    RoomAvatarEventContent instance) =>
    <String, dynamic>{
      'rawContent': instance.rawContent,
      'info': instance.info,
      'url': instance.url?.toString()
    };

RoomAvatarEvent _$RoomAvatarEventFromJson(Map<String, dynamic> json) {
  return RoomAvatarEvent()
    ..type = json['type'] as String
    ..eventID = json['event_id'] as String
    ..refreshToken = json['prev_content'] as Map<String, dynamic>
    ..originServerTs = json['origin_server_ts'] as int
    ..sender = json['sender'] as String
    ..stateKey = json['state_key'] as String
    ..roomID = json['room_id'] as String
    ..unsignedData = json['unsigned'] == null
        ? null
        : UnsignedData.fromJson(json['unsigned'] as Map<String, dynamic>)
    ..redacts = json['redacts'] as String
    ..content = json['content'] == null
        ? null
        : RoomAvatarEventContent.fromJson(
        json['content'] as Map<String, dynamic>);
}

Map<String, dynamic> _$RoomAvatarEventToJson(RoomAvatarEvent instance) =>
    <String, dynamic>{
      'type': instance.type,
      'event_id': instance.eventID,
      'prev_content': instance.refreshToken,
      'origin_server_ts': instance.originServerTs,
      'sender': instance.sender,
      'state_key': instance.stateKey,
      'room_id': instance.roomID,
      'unsigned': instance.unsignedData,
      'redacts': instance.redacts,
      'content': instance.content
    };
