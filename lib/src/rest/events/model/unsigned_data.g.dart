// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unsigned_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UnsignedData _$UnsignedDataFromJson(Map<String, dynamic> json) {
  return UnsignedData()
    ..age = json['age'] as int
    ..redactedEvent = json['redacted_because'] == null
        ? null
        : Event.fromJson(json['redacted_because'] as Map<String, dynamic>)
    ..transactionID = json['transaction_id'] as String
    ..prevContent = json['prev_content'] as Map<String, dynamic>;
}

Map<String, dynamic> _$UnsignedDataToJson(UnsignedData instance) =>
    <String, dynamic>{
      'age': instance.age,
      'redacted_because': instance.redactedEvent,
      'transaction_id': instance.transactionID,
      'prev_content': instance.prevContent
    };
