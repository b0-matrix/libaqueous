import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event_type.dart';
import 'package:libaqueous/src/rest/events/model/room_avatar_event.dart';
import 'package:libaqueous/src/rest/events/model/room_canonical_alias_event.dart';
import 'package:libaqueous/src/rest/events/model/room_member_event.dart';
import 'package:libaqueous/src/rest/events/model/room_message_event.dart';
import 'package:libaqueous/src/rest/events/model/room_name_event.dart';
import 'package:libaqueous/src/rest/events/model/room_topic_event.dart';
import 'package:libaqueous/src/rest/events/model/unsigned_data.dart';

part 'event.g.dart';

@JsonSerializable()
class EventContent {
  Map<String, dynamic> rawContent;

  static EventContent fromJson(Map<String, dynamic> json) {
    return EventContent()
      ..rawContent = json;
  }

  Map<String, dynamic> toJson() => rawContent;
}

@JsonSerializable()
class Event {
  @JsonKey(name: "type")
  String type;

  @JsonKey(name: "event_id")
  String eventID;

  @JsonKey(name: "content")
  EventContent content;

  @JsonKey(name: "prev_content")
  Map<String, dynamic> refreshToken;

  @JsonKey(name: "origin_server_ts")
  int originServerTs;

  @JsonKey(name: "sender")
  String sender;

  @JsonKey(name: "state_key")
  String stateKey;

  @JsonKey(name: "room_id")
  String roomID;

  @JsonKey(name: "unsigned")
  UnsignedData unsignedData;

  @JsonKey(name: "redacts")
  String redacts;

  @JsonKey(ignore: true)
  String mToken;

  String toString() => jsonEncode(toJson());

  static Event fromJson(Map<String, dynamic> json) {
    switch (json["type"] ?? "") {
      case EventType.STATE_ROOM_AVATAR:
        return RoomAvatarEvent.fromJson(json);
      case EventType.STATE_CANONICAL_ALIAS:
        return RoomCanonicalAliasEvent.fromJson(json);
      case EventType.STATE_ROOM_MEMBER:
        return RoomMemberEvent.fromJson(json);
      case EventType.MESSAGE:
        return RoomMessageEvent.fromJson(json);
      case EventType.STATE_ROOM_NAME:
        return RoomNameEvent.fromJson(json);
      case EventType.STATE_ROOM_TOPIC:
        return RoomTopicEvent.fromJson(json);
    }
    return _$EventFromJson(json);
  }

  Map<String, dynamic> toJson() => _$EventToJson(this);
}
