import 'package:json_annotation/json_annotation.dart';
import 'package:libaqueous/src/rest/events/model/event.dart';

part 'tokens_chunk_events.g.dart';

@JsonSerializable()
class TokensChunkEvents {
  String start;
  String end;

  List<Event> events;
}
