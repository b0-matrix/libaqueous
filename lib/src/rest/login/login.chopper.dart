// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

class _$LoginAPI extends LoginAPI {
  _$LoginAPI([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  final definitionType = LoginAPI;

  Future<Response<Credential>> postLogin(
      LoginType type,
      UserIdentifier identifier,
      String password,
      String token,
      String deviceID,
      bool initialDeviceDisplayName) {
    final $url = '/_matrix/client/r0/login';
    final Map<String, dynamic> $params = {
      'type': type,
      'identifier': identifier,
      'password': password,
      'token': token,
      'device_id': deviceID,
      'initial_device_display_name': initialDeviceDisplayName
    };
    final $request = Request('POST', $url, client.baseUrl, parameters: $params);
    return client.send<Credential, Credential>($request);
  }

  Future<Response> postLogout() {
    final $url = '/_matrix/client/r0/logout';
    final $request = Request('POST', $url, client.baseUrl);
    return client.send<dynamic, dynamic>($request);
  }
}
