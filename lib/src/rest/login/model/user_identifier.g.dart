// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_identifier.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserIdentifier _$UserIdentifierFromJson(Map<String, dynamic> json) {
  return UserIdentifier()..type = json['type'] as String;
}

Map<String, dynamic> _$UserIdentifierToJson(UserIdentifier instance) =>
    <String, dynamic>{'type': instance.type};
