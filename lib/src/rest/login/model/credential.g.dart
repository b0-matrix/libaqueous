// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credential.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Credential _$CredentialFromJson(Map<String, dynamic> json) {
  return Credential()
    ..userID = json['user_id'] as String
    ..homeServer = json['home_server'] as String
    ..accessToken = json['access_token'] as String
    ..refreshToken = json['refresh_token'] as String
    ..deviceID = json['device_id'] as String;
}

Map<String, dynamic> _$CredentialToJson(Credential instance) =>
    <String, dynamic>{
      'user_id': instance.userID,
      'home_server': instance.homeServer,
      'access_token': instance.accessToken,
      'refresh_token': instance.refreshToken,
      'device_id': instance.deviceID
    };
