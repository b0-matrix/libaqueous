import 'package:json_annotation/json_annotation.dart';

enum LoginType {
  @JsonKey(name: "m.login.password")
  M_LOGIN_PASSWORD,
  @JsonKey(name: "m.login.token")
  M_LOGIN_TOKEN
}
