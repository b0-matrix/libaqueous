import 'package:json_annotation/json_annotation.dart';

part 'user_identifier.g.dart';

@JsonSerializable()
class UserIdentifier {
  @JsonKey(name: "type")
  String type;

  static const fromJson = _$UserIdentifierFromJson;

  Map<String, dynamic> toJson() => _$UserIdentifierToJson(this);
}
