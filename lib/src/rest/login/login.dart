import "dart:async";

import 'package:chopper/chopper.dart';
import 'package:libaqueous/src/rest/const.dart';
import 'package:libaqueous/src/rest/login/model/credential.dart';
import 'package:libaqueous/src/rest/login/model/login_type.dart';
import 'package:libaqueous/src/rest/login/model/user_identifier.dart';

part "login.chopper.dart";

@ChopperApi(baseUrl: MATRIX_CLIENT_R0)
abstract class LoginAPI extends ChopperService {
  static LoginAPI create([ChopperClient client]) => _$LoginAPI(client);

  @Post(path: "/login")
  Future<Response<Credential>> postLogin(
      @Query("type") LoginType type,
      @Query("identifier") UserIdentifier identifier,
      @Query("password") String password,
      @Query("token") String token,
      @Query("device_id") String deviceID,
      @Query("initial_device_display_name") bool initialDeviceDisplayName);

  @Post(path: "/logout")
  Future<Response> postLogout();
}
