import 'dart:io' show Platform;

import 'package:libaqueous/src/internal/default_client.dart';
import 'package:test/test.dart';

void main() {
  final String homeserver = Platform.environment['HOMESERVER'];
  final String username = Platform.environment['USERNAME'];
  final String token = Platform.environment['TOKEN'];

  group('Test Matrix Client', () {
    DefaultClient client;

    setUp(() async {
      client = DefaultClient(homeserver, username, token);
      await client.load(Uri.parse("/tmp/libaqueous/"));
    });

    test('Test Client Sync', () async {
      client.startSync();
      await Future.delayed(Duration(seconds: 30));
      client.stopSync();
    }, timeout: Timeout(Duration(days: 1)));
  });
}
