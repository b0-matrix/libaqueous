# libaqueous

A Matrix SDK in Dart!

Currently it targets Flutter, web and Dart VM platforms.

# Usage

1. Set up store

```dart
final store = SembastStore();
await store.open();
```

2. Construct and load client

```dart
final client = Client(homeserver, username, token, store);
await client.loadFromStore();
```

3. Register handler

```dart
EventHandler.eventBus.on<RoomNewEvent>().listen((e) {
  print(e.event);
});
```

4. Sync

```dart
client.startSync();
```

# Features and Bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://gitlab.com/b0/libaqueous/issues
